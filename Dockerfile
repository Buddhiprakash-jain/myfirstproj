FROM python:latest

WORKDIR /app

ADD . /app

RUN pip install flask

CMD ["python" , "app.py"]
